<?php

namespace Ootliers\Subscriber;

use Ootliers\Transport\OrderTransporter;
use Shopware\Core\Checkout\Order\Event\OrderStateMachineStateChangeEvent;
use Shopware\Core\Checkout\Order\OrderEvents;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityLoadedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderSubscriber implements EventSubscriberInterface
{
    private $orderTransporter;

    public function __construct(OrderTransporter $orderTransporter)
    {
        $this->orderTransporter = $orderTransporter;
    }

    public static function getSubscribedEvents()
    {
        return [
            'state_enter.order_transaction.state.paid' => 'processOrder',
        ];
    }

    public function processOrder(OrderStateMachineStateChangeEvent $entityWrittenEvent) {
        $order = $entityWrittenEvent->getOrder();
        $this->orderTransporter->transport($order);
    }
}
