<?php

namespace Ootliers\Transport;

use GuzzleHttp\ClientInterface;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemCollection;
use Shopware\Core\Checkout\Order\Aggregate\OrderLineItem\OrderLineItemEntity;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class OrderTransporterTest extends TestCase
{
    const ORDER_NUMBER = "1001";
    const TOTAL_PRICE = 20.21;

    const ITEM_DESCRIPTION = 'An item';
    const ITEM_PRICE = 20.21;
    const ITEM_QUANTITY = 2;

    const SITE_ID = 'kldsfjhdskljhf';
    const API_KEY = 'kldsfjhdsklfjds';

    public function testIfThisFails() {

        $clientStub = $this->createMock(ClientInterface::class);
        $systemConfig = $this->createMock(SystemConfigService::class);

        $systemConfig->method('get')->will(
            $this->returnValueMap(
                [
                    ['OotliersPlugin.config.siteId', null, self::SITE_ID],
                    ['OotliersPlugin.config.apiKey', null, self::API_KEY]
                ]
            )
        );

        $cartPrice = $this->createMock(CartPrice::class);
        $linePrice = $this->createMock(CalculatedPrice::class);


        $cartPrice->method('getTotalPrice')->willReturn(self::TOTAL_PRICE);
        $linePrice->method('getTotalPrice')->willReturn(self::ITEM_PRICE);

        $line = $this->createMock(OrderLineItemEntity::class);
        $line->method('getPrice')->willReturn($linePrice);
        $line->method('getDescription')->willReturn(self::ITEM_DESCRIPTION);
        $line->method('getQuantity')->willReturn(self::ITEM_QUANTITY);

        $lineCollection = $this->createMock(OrderLineItemCollection::class);
        $lineCollection->method('getElements')->willReturn([$line]);

        $now = new \DateTime("now");
        $orderEntity = $this->createStub(OrderEntity::class);
        $orderEntity->method('getLineItems')->willReturn($lineCollection);
        $orderEntity->method('getOrderNumber')->willReturn(self::ORDER_NUMBER);
        $orderEntity->method('getCreatedAt')->willReturn($now);
        $orderEntity->method('getPrice')->willReturn($cartPrice);

        $expectedOutput = [
            'order' => [
                'number' => self::ORDER_NUMBER,
                'ordered_at' => $now->format(\DATE_ATOM),
                'total' => (string) self::TOTAL_PRICE,
                'items' => [
                    [
                        'name' => self::ITEM_DESCRIPTION,
                        'price' => (string) self::ITEM_PRICE,
                        'quantity' => self::ITEM_QUANTITY,
                    ]
                ]
            ]
        ];
        $url = sprintf(OrderTransporter::URL, self::SITE_ID);

        $clientStub->expects($this->once())->method('request')->with(
            $this->equalTo('post'),
            $this->equalTo($url), $this->equalTo([
            'json' => $expectedOutput,
            'headers' => ['Authorization' => self::API_KEY],
        ]));

        $transporter = new OrderTransporter($systemConfig, $clientStub);
        $transporter->transport($orderEntity);
    }
}
