<?php

namespace Ootliers\Command;

use Ootliers\Transport\OrderTransporter;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCommand extends Command
{
    protected static $defaultName = 'ootliers:export';
    private $orderRepository;
    /**
     * @var OrderTransporter
     */
    private $orderTransporter;

    public function __construct(EntityRepositoryInterface $orderRepository, OrderTransporter $orderTransporter)
    {
        parent::__construct(null);
        $this->orderRepository = $orderRepository;
        $this->orderTransporter = $orderTransporter;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $orders = $this->orderRepository->search(new Criteria([]), \Shopware\Core\Framework\Context::createDefaultContext());
        /** @var OrderEntity $order */
        foreach ($orders as $order) {
            $this->orderTransporter->transport($order);
        }

        return 0;
    }
}
