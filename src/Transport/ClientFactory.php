<?php

namespace Ootliers\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

class ClientFactory
{
    public function getClient() : ClientInterface
    {
        return new Client();
    }
}
