<?php

namespace Ootliers\Transport;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Order\OrderEntity;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class OrderTransporter
{
    public const URL = "https://api.ootliers.com/api/v1/site/%s/order";

    /**
     * @var Client
     */
    private $client;
    /**
     * @var SystemConfigService
     */
    private $systemConfigService;

    public function __construct(SystemConfigService $systemConfigService, ClientInterface $client) {
        $this->client = $client;
        $this->systemConfigService = $systemConfigService;
    }

    public function transport(OrderEntity $orderEntity) {

        $items = [];
        if ($orderEntity->getLineItems() !== null) {
            /** @var LineItem $lineItem */
            foreach ($orderEntity->getLineItems()->getElements() as $lineItem) {
                $items[] = [
                    'name' => $lineItem->getDescription(),
                    'price' => (string) $lineItem->getPrice()->getTotalPrice(),
                    'quantity' => $lineItem->getQuantity(),
                ];
            }
        }

        $output = [
            'order' => [
                'number' => $orderEntity->getOrderNumber() ?? 'n/a',
                'ordered_at' => $orderEntity->getCreatedAt()->format(\DATE_ATOM),
                'total' => (string) $orderEntity->getPrice()->getTotalPrice(),
                'items' => $items,
            ],
        ];

        $url = sprintf(self::URL, $this->systemConfigService->get('OotliersPlugin.config.siteId'));

        $this->client->request('post', $url, [
            'json' => $output,
            'headers' => ['Authorization' => $this->systemConfigService->get('OotliersPlugin.config.apiKey')]
        ]);
    }
}
